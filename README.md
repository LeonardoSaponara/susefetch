Susefetch
==========

A port of [Paleofetch](https://github.com/ss7m/paleofetch) for OpenSuse distros. Created without prior knowledge of C, by the way. ~~No wonder the code probably sucks~~

Paleofetch is a rewrite of [Neofetch](https://github.com/dylanaraps/neofetch) in C.

Differences between Susefetch and Paleofetch:
------------
* Added OpenSuse logo

* Removed Battery Information

* Ported Packages Information from pacman to RPM

* Added screen refresh rate to Resolution Information

* Changed default configuration

Why use Susefetch over Neofetch?
-----------------------------------------

The  major reason is performance improvement. 

Here the real elapsed time (on my machine, YMMV) averaged on 100 runs: 

Packages Info enabled:
    
&emsp;Neofetch: 0.9435s 

&emsp;Susefetch (recached): 0.6498s&emsp;**31.83% decrease** in elapsed time

&emsp;Susefetch (cached): 0.62572s&emsp;**33.68% decrease** in elapsed time

Packages Info disabled:

&emsp;Neofetch: 0.22169s 

&emsp;Susefetch (recached): 0.01013s&emsp;**95.43% decrease** in elapsed time

&emsp;Susefetch (cached): 0.0032s&emsp;**98.56% decrease** in elapsed time

Example Image
------------

![example output](example.png)

Dependencies
------------
To run, Susefetch requires `libX11` and `libpci` which are already included in OpenSuse (Both Tumbleweed and Leap).

To compile, instead, you also need to install `libX11-devel` and `pciutils-devel`

Compiling
---------

    make install

Usage
-----

After compiling, simply run the executable:

    susefetch

By default, `susefetch` will cache certain  information (in `$XDG_CACHE_HOME/susefetch`)
to speed up subsequent calls. To ignore the contents of the cache (and repopulate it), run

    susefetch --recache

The cache file can safely be removed at any time, Susefetch will repopulate it if it is absent.

Configuration
-------------

Susefetch is configured by editing `config.h` and recompiling.
You can change your logo by including the appropriate header file in the logos' directory.
The color with which Susefetch fetch draws the logo can be chosen by defining the `COLOR` macro,
look up ANSI escape codes for information on customizing this.

The last configuration is the `CONFIG` macro, which controls what information Susefetch
prints. Each entry in this macro should look like

    { "NAME: ",   getter_function, false }, \
    
Take note of the trailing comma and backslash. The first piece, `"NAME: "`, sets
what Susefetch prints before printing the information; this usually tells you what
bit of information is being shown. Note that the name entry should be unique for entries
which are to be cached. The second piece, `getter_function`, sets
which function Susefetch will call display. Current available getter functions are

* `get_title`: prints `host@user` like in a bash prompt. Host and user will be printed in color.
* `get_bar`: Meant to be added after `get_title`, underlines the title
* `get_os`: Prints your operating system (including distribution)
* `get_host`: Prints the model of computer
* `get_kernel`: Prints the version of the linux kernel
* `get_uptime`: Shows how long linux has been running
* `get_packages`: Shows how many packages you have installed. Works only for rpm packages (so Zypper/Yast).
* `get_shell`: Shows which shell you are using
* `get_resolution`: Prints your screen resolution
* `get_terminal`: Prints the name of your current terminal
* `get_cpu`: Prints the name of your CPU, number of cores, and maximum frequency
* `get_gpu1`, `get_gpu2`: Print the GPU on your system. If you don't have both integrated graphics and an external GPU, `get_gpu2` will likely be blank
* `get_gpu`: (Tries to) print your current GPU
* `get_colors1`, `get_colors2`: Prints the colors of your terminal

To include a blank line between entries, put `SPACER \` between the two lines
you want to separate.

The booleans in `CONFIG` tell Susefetch whether you want to cache an entry.
When cached, Susefetch will save the value and not recompute it whenever you run Susefetch
(unless you specify the `--recache` option).

The CPU and GPU name can be configured as well. This is done under the CPU_CONFIG and GPU_CONFIG section
in the config.h file. Two macros are provided to customize and tidy up the model names:

* `REMOVE(string)`: removes the first occurrence of `string`
* `REPLACE(string1, string2)`: replaces the first occurrence of `string1` with `string2`

Don't forget to run Susefetch with the --recache flag after compiling it with your new
configuration, otherwise it will still show the old name for already cached entries.

FAQ
---

**Q:** I Like gambling, what have you got for me?  
**A:** In paleofetch.c replace `xrandr --nograb --current` with just `xrandr` and then re-compile.

**Q:** Why Susefetch, Neofetch and similar utilities are so slow in OpenSuse?  
**A:** RPM-based distributions like OpenSuse cache their installed packages list in a database that is quite slow to query. 
