CFLAGS=-O2 -Wall -Wextra -lX11 -lpci
PREFIX=$(HOME)/.local
CACHE=$(shell if [ "$$XDG_CACHE_HOME" ]; then echo "$$XDG_CACHE_HOME"; else echo "$$HOME"/.cache; fi)

all: susefetch

clean:
	rm -f susefetch $(CACHE)/susefetch

susefetch: paleofetch.c paleofetch.h config.h
	$(CC) paleofetch.c -o susefetch $(CFLAGS) 
	strip susefetch

install: susefetch
	mkdir -p $(PREFIX)/bin
	install ./susefetch $(PREFIX)/bin/susefetch
